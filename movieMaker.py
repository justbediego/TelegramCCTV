import cv2
import numpy as np
import glob

inputPath = 'C:/Users/diego/Desktop/kkk/photos/*_thumb.jpg'
outputPath = 'C:/Users/diego/Desktop/result.mp4'
out = None
files = glob.glob(inputPath)
textFont = cv2.FONT_HERSHEY_PLAIN
textPosition = (10, 20)
fontColor = (0, 0, 255)


def getTextDate(filename):
    part = filename.split('@')[1]
    return part[6:10] + "-" + part[3:5] + "-" + part[0:2] + " " + part[11:13]+":"+part[14:16]+":"+part[17:19]


def photoIndex(filename):
    return int(filename.split('photo_')[1].split('@')[0])


fromIndex = 77353
toIndex = 79288

files.sort(key=photoIndex)

count = 0
for filename in files:
    if(photoIndex(filename) < fromIndex or photoIndex(filename) > toIndex):
        continue
    img = cv2.imread(filename.replace("_thumb", ""))
    cv2.putText(img, getTextDate(filename),textPosition, textFont, 1, fontColor, 1)
    height, width, layers = img.shape
    size = (width, height)
    if (out == None):
        out = cv2.VideoWriter(outputPath, cv2.VideoWriter_fourcc(*'DIVX'), 25, size)
    out.write(img)
    count = count + 1
    print(str(count) + "/" + str(toIndex-fromIndex))

out.release()
