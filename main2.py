from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import threading
import time
import json
import cv2 

CAMERA_ID = 1 + cv2.CAP_DSHOW


#DB
def writeDB(data):
    outfile = open('database\db2.json', 'w')
    json.dump(data, outfile)

def readDB():
    outfile = open('database\db2.json', 'r')
    return json.load(outfile)

#Camera
def takePicture():
    cam = cv2.VideoCapture(CAMERA_ID)
    # cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    # cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        
    # lighting issue
    for _ in range(30):
        result, image = cam.read()
        time.sleep(.01)
    if not result:
        raise Exception("Cannot take picture, no result")
    
    cv2.imwrite("images/taken2.jpeg", image)
    cam.release()
    del cam

#telegram
updater = Updater(token='2031266401:AAHPJKPboK9bOzINUMoX7PgN_3kTX32HUYg')
dispatcher = updater.dispatcher
authorizedPeople=readDB()

#Engine
def engine():
    while(True):
        if(authorizedPeople):
            try:
                takePicture()
                for chat_id in authorizedPeople:
                    dispatcher.bot.send_photo(chat_id=chat_id, photo=open('images/taken2.jpeg', 'rb'))
            except Exception as err:
                print(F"Something failed this time: {err}")
        time.sleep(30)

def start(update, context):
    bot = context.bot
    bot.send_message(chat_id=update.message.chat_id, text="Hey there, you know what to do")

def stop(update, context):
    bot = context.bot
    bot.send_message(chat_id=update.message.chat_id, text="Ok man, it will be lonely without you")
    while(update.message.chat_id in authorizedPeople):
        authorizedPeople.remove(update.message.chat_id)
        writeDB(authorizedPeople)
    if(not authorizedPeople):
        bot.send_message(chat_id=update.message.chat_id, text="Oh btw, no one else is watching")

def password(update, context):
    bot = context.bot
    if update.message.text=="Shide and Hossein are awesome":
        bot.send_message(chat_id=update.message.chat_id, text="Hell yea they are")
        if(update.message.chat_id not in authorizedPeople):
            authorizedPeople.append(update.message.chat_id)
            writeDB(authorizedPeople)
    else:
        bot.send_message(chat_id=update.message.chat_id, text="What?")

#handlers
dispatcher.add_handler(CommandHandler('start', start))
dispatcher.add_handler(CommandHandler('stop', stop))
dispatcher.add_handler(MessageHandler(Filters.text, password))
#start engine
if __name__ == '__main__':
    e=threading.Thread(target=engine)
    e.start()
#start bot
updater.start_polling()

